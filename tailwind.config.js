const defaultTheme = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            colors:{
                dark_mode_main: '#2F3348',
                dark_mode_background: '#25293C',
                dark_mode_text: '#ADB0CC',
                dark_mode_hover: '#34384C',
                dark_mode_second_text: '#73778F',
                // dark_mode_header_text: '#CCCFDD',
                light_mode_main: '#FFFFFF',
                light_mode_background: '#F8F7FA',
                light_mode_text: '#5C5A68',
                // light_mode_header_text: '#CCCFDD',

            },
            boxShadow: {
                auth: '0 3px 9px 1px rgba(12, 16, 27, 0.15), 0 9px 8px rgba(12, 16, 27, 0.01), 0 1px 6px 4px rgba(12, 16, 27, 0.08)'
            },
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
            },
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
