import { createI18n } from 'vue-i18n'

const locale = localStorage.getItem('lang') ? localStorage.getItem('lang') : 'en'

export default createI18n({
    legacy: false,
    locale: locale,
    globalInjection: true,
    // messages
})
